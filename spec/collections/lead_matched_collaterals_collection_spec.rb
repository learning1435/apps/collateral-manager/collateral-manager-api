require 'faker'
require 'rails_helper'

RSpec.describe ::LeadMatchedCollateralsCollection do
  context '.get' do
    it 'return collaterals assigned to lead' do
      tag_1 = Tag.create(name: "Tag 1", color: Faker::Color.hex_color)
      collateral_kind_1 = CollateralKind.create(name: "prototype", color: Faker::Color.hex_color)
      lead_1 = Lead.create(name: "Lead 1", description: "Description")
      lead_1.lead_tags.create(position: 0, tag: tag_1)
      collateral_1 = Collateral.create(name: "Collateral 1", collateral_kind: collateral_kind_1, url: Faker::Internet.url(host: Faker::Internet.unique.domain_name))
      collateral_1.collateral_tags.create(weight: 10, tag: tag_1)

      result = LeadMatchedCollateralsCollection.new(lead_1).get

      expect([collateral_1]).to eq result
    end

    it 'return collaterals assigned to lead' do
      tag_1 = Tag.create(name: "Tag 1", color: Faker::Color.hex_color)
      collateral_kind_1 = CollateralKind.create(name: "prototype", color: Faker::Color.hex_color)
      lead_1 = Lead.create(name: "Lead 1", description: "Description")
      lead_1.lead_tags.create(position: 0, tag: tag_1)
      collateral_1 = Collateral.create(name: "Collateral 1", collateral_kind: collateral_kind_1, url: Faker::Internet.url(host: Faker::Internet.unique.domain_name))
      collateral_1.collateral_tags.create(weight: 10, tag: tag_1)

      result = LeadMatchedCollateralsCollection.new(lead_1).get

      expect([collateral_1]).to eq result
    end
  end
end